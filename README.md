# README #

Clone repository and add it Android Studio to run application on android Emulator or Device.

### What is this repository for? ###

This project repository is for currency conversion Android app from one currency type to another.

### How do I get set up? ###

* Import project in Android studio using git option from:
git clone https://ajeetahead@bitbucket.org/ajeetahead/currency.git

* Dependencies
Sync dependencies using gradle
